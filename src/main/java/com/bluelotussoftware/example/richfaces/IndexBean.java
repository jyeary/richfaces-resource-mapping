/*
 * Copyright 2013 Blue Lotus Software, LLC..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.example.richfaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Example page backing bean.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
@RequestScoped
public class IndexBean {

    /**
     * Title for Page.
     */
    private final String title;

    /**
     * Default constructor.
     */
    public IndexBean() {
        title = "RichFaces 4.3.x Resource Mapping Example";
    }

    /**
     * Getter for title
     *
     * @return {@link String} value of the page title.
     */
    public String getTitle() {
        return title;
    }

}
